package be.com.jera.dojodi

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.mockk
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

class SignUpViewModelTest : KoinTest {

    private val signUpViewModelTest: SignUpViewModel by inject()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        @JvmStatic
        @BeforeClass
        fun `should inject my components`() {
            val applicationTest: DojoDIApplication = mockk()
            val repositoryTest = mockk<SignUpRespository>()
            startKoin {
                androidContext(applicationTest)
                modules(
                    module {
                        factory { repositoryTest }
                    }
                )
                modules(DiManager.viewModelsModules)
            }
        }
    }

    @Test
    fun `should show cpf if account type is LEGAL_PERSON`() {
        val viewModel = signUpViewModelTest

        viewModel.accountType = AccountType.LEGAL_PERSON

        assert(viewModel.state.value?.showCPF == true)
    }

    @Test
    fun `should not show cpf when account type is LEGAL_ENTITY`() {
        val viewModel = signUpViewModelTest

        viewModel.accountType = AccountType.LEGAL_ENTITY

        assert(viewModel.state.value?.showCPF == false)
    }

    @Test
    fun `should show cnpj when account type is LEGAL_ENTITY`() {
        val viewModel = signUpViewModelTest

        viewModel.accountType = AccountType.LEGAL_ENTITY

        assert(viewModel.state.value?.showCNPJ == true)
    }

    @Test
    fun `should not show cnpj when account type is LEGAL_PERSON`() {
        val viewModel = signUpViewModelTest

        viewModel.accountType = AccountType.LEGAL_PERSON

        assert(viewModel.state.value?.showCNPJ == false)
    }

}