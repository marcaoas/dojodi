package be.com.jera.dojodi

import android.app.Application
import org.koin.core.context.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModel

object DiManager {

    fun startDI(application: Application) {
        startKoin {
            // declare used Android context
            androidContext(application)
            // declare modules
            modules(viewModelsModules)
            modules(respositoriesModules)
        }
    }

    private val respositoriesModules
        get() = module {
            factory { SignUpRespository() }
        }

    val viewModelsModules
        get() = module {
            viewModel { SignUpViewModel(get()) }
        }

}