package be.com.jera.dojodi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.view.isVisible
import be.com.jera.dojodi.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: SignUpViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        viewModel.state.observe(this, {
            setViewState(binding, it)
        })

        binding.signUpPfButton.setOnClickListener {
            viewModel.accountType = AccountType.LEGAL_PERSON
        }

        binding.signUpPjButton.setOnClickListener {
            viewModel.accountType = AccountType.LEGAL_ENTITY
        }

    }

    private fun setViewState(binding: ActivityMainBinding, state: ViewState) {
        binding.signUpSaveButton.isEnabled = state.saveButtonEnabled
        binding.signUpCnpjEditText.isVisible = state.showCNPJ
        binding.signUpCpfEditText.isVisible = state.showCPF
        binding.signUpUserNameEditText.isVisible = state.showUserName
        binding.signUpCompanyNameEditText.isVisible = state.showCompanyName
    }
}