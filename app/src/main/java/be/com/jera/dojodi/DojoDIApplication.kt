package be.com.jera.dojodi

import android.app.Application

class DojoDIApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DiManager.startDI(this)
    }

}