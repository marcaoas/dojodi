package be.com.jera.dojodi

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SignUpViewModel(val signUpRespository : SignUpRespository) : ViewModel() {

    var accountType: AccountType = AccountType.LEGAL_PERSON
        set(value) {
            field = value
            _state.value = ViewState(
                showUserName = value == AccountType.LEGAL_PERSON,
                showCPF =  value == AccountType.LEGAL_PERSON,
                showCNPJ = value == AccountType.LEGAL_ENTITY,
                showCompanyName = value == AccountType.LEGAL_ENTITY,
            )
        }


    private val _state: MutableLiveData<ViewState> = MutableLiveData(create(accountType))
    val state: LiveData<ViewState> get() = _state

    fun create(account: AccountType): ViewState {
        return ViewState(
            saveButtonEnabled = false,
            showCompanyName = false,
            showCNPJ = false,
            showCPF = false,
            showUserName = false
        )
    }

}

enum class AccountType {
    LEGAL_ENTITY, LEGAL_PERSON
}

data class ViewState(
    val saveButtonEnabled: Boolean = false,
    val showCompanyName: Boolean = false,
    val showCNPJ: Boolean = false,
    val showCPF: Boolean = false,
    val showUserName: Boolean = false
)